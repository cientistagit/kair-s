package util;

import java.sql.Connection;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.internal.SessionImpl;
import org.hibernate.service.ServiceRegistry;

public class Fabrica {
    
    private static final SessionFactory sessionFactory;
    private static final ServiceRegistry serviceRegistry;
    private static final Configuration configuration;
    private static Session secao;

    
    static {
        configuration = new Configuration().configure();
        serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        sessionFactory = configuration.configure().buildSessionFactory(serviceRegistry);
    }

    //para poder mexer nos relatórios - jasper
    public static Connection getConnection(){
        Session session = getSession();
        SessionImpl sessionImp = (SessionImpl) session;
        return sessionImp.connection(); 
    }
    
    public static Session getSession() {
        return sessionFactory.openSession();
    }

}