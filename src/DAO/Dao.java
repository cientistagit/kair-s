package DAO;

import javax.swing.JOptionPane;
import org.hibernate.Session;
import util.Fabrica;

public class Dao<T> {

    Session sessao = Fabrica.getSession();

    public void salvar(T t) {
        try {
            sessao.beginTransaction();
            sessao.saveOrUpdate(t);
            sessao.getTransaction().commit();
            if (sessao.isOpen()) {
                sessao.close();
            }

        } catch (ExceptionInInitializerError e) {
            if (sessao.isOpen()) {
                sessao.close();
            }
        }
    }

    public void salvar(T t, boolean fecharSessao) {
        try {
            sessao.beginTransaction();
            sessao.saveOrUpdate(t);
            sessao.getTransaction().commit();
            if (fecharSessao == true) {
                sessao.close();
            }

        } catch (ExceptionInInitializerError e) {
            if (sessao.isOpen()) {
                sessao.close();
            }
        }
    }

    public void atualizar(T t) {
        try {
            sessao.getTransaction().begin();
            sessao.update(t);
            sessao.getTransaction().commit();
            if (sessao.isOpen()) {
                sessao.close();
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Não foi possivel atualizar os dados.");
            sessao.getTransaction().rollback();
        }
    }

    public void remover(T t) {
        try {
            sessao.getTransaction().begin();
            sessao.delete(t);
            sessao.getTransaction().commit();
            if (sessao.isOpen()) {
                sessao.close();
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Os Dados não foram removidos");
            sessao.getTransaction().rollback();
        }
    }

    public void fecharSesao() {
        if (sessao.isOpen()) {
            sessao.close();
        }

    }
}
