package modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "aula")
public class Aula {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private int id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Oferta_id", nullable = false)
    private Oferta oferta;

    @Column(name = "diadasemana", nullable = false, length = 45)
    private String diadasemana;

    @Column(name = "hora", nullable = false)
    private Long hora;

    @Column(name = "duracao", nullable = false)
    private Long duracao;

    @Column(name = "sala", nullable = false)
    private int sala;

    public Aula() {
    }

    public Aula(int id, Oferta oferta, String diadasemana, Long hora, Long duracao, int sala) {
        this.id = id;
        this.oferta = oferta;
        this.diadasemana = diadasemana;
        this.hora = hora;
        this.duracao = duracao;
        this.sala = sala;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Oferta getOferta() {
        return oferta;
    }

    public void setOferta(Oferta oferta) {
        this.oferta = oferta;
    }

    public String getDiadasemana() {
        return diadasemana;
    }

    public void setDiadasemana(String diadasemana) {
        this.diadasemana = diadasemana;
    }

    public Long getHora() {
        return hora;
    }

    public void setHora(Long hora) {
        this.hora = hora;
    }

    public Long getDuracao() {
        return duracao;
    }

    public void setDuracao(Long duracao) {
        this.duracao = duracao;
    }

    public int getSala() {
        return sala;
    }

    public void setSala(int sala) {
        this.sala = sala;
    }

    @Override
    public String toString() {
        return "Aula{" + "id=" + id + ", oferta=" + oferta + ", diadasemana=" + diadasemana + ", hora=" + hora + ", duracao=" + duracao + ", sala=" + sala + '}';
    }

}
