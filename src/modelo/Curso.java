package modelo;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
        @Table(name = "curso")
        @NamedQueries({
    @NamedQuery(name = "todosOsCursos", query = "from Curso"),
    
        @NamedQuery(name = "disciplinasPorCurso", query = "select disc from Curso cur left join cur.disciplinas disc where cur.id = ?")


})

public class Curso {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private int id;

    @Column(name = "descricao", nullable = false)
    private String descricao;

    @OneToMany(/*fetch = FetchType.LAZY, */mappedBy = "curso")
    private List<Disciplina> disciplinas;

    public Curso() {
    }

    public Curso(int id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public Curso(int id, String descricao, List<Disciplina> disciplinas) {
        this.id = id;
        this.descricao = descricao;
        this.disciplinas = disciplinas;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<Disciplina> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(List<Disciplina> disciplinas) {
        this.disciplinas = disciplinas;
    }

    @Override
    public String toString() {
        return descricao;
    }

}
