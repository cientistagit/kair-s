package modelo;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "oferta")
@NamedQueries({
    @NamedQuery(name = "todasAsOfertas", query = "from Oferta"),
    @NamedQuery(name = "ofertasPorId", query = "from Oferta where id = ?")
})
public class Oferta {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private int id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Disciplina_id", nullable = false)
    private Disciplina disciplina;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "professor_id", nullable = false)
    private Professor professor;

    @Column(name = "semestre", nullable = false)
    private Integer semestre;

    @Column(name = "turno", nullable = false, length = 45)
    private String turno;

    @Column(name = "turma", nullable = false, length = 45)
    private String turma;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "oferta")
    private List<Aula> aulas;

    public Oferta() {
    }

    public Oferta(int id, Disciplina disciplina, Professor professor, int semestre, String turno, String turma, List<Aula> aulas) {
        this.id = id;
        this.disciplina = disciplina;
        this.professor = professor;
        this.semestre = semestre;
        this.turno = turno;
        this.turma = turma;
        this.aulas = aulas;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Disciplina getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(Disciplina disciplina) {
        this.disciplina = disciplina;
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    public int getSemestre() {
        return semestre;
    }

    public void setSemestre(int semestre) {
        this.semestre = semestre;
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public String getTurma() {
        return turma;
    }

    public void setTurma(String turma) {
        this.turma = turma;
    }

    public List<Aula> getAulas() {
        return aulas;
    }

    public void setAulas(List<Aula> aulas) {
        this.aulas = aulas;
    }

    @Override
    public String toString() {
        return "Oferta{" + "id=" + id + ", disciplina=" + disciplina + ", professor=" + professor + ", semestre=" + semestre + ", turno=" + turno + ", aulas=" + aulas + '}';
    }

}
