package modelo;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "disciplina")
@NamedQueries( {
    @NamedQuery(name = "todasAsDisciplinas", query = "from Disciplina")    
})
public class Disciplina {

    //ATRIBUTOS COM MAPEAMENTOS
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private int id;

    @ManyToOne(/*fetch = FetchType.LAZY*/)
    @JoinColumn(name = "Curso_id", nullable = false)
    private Curso curso;

    @Column(name = "descricao", nullable = false)
    private String descricao;

    @Column(name = "cargahoraria", nullable = false)
    private Long cargahoraria;

    @Column(name = "cargahorariasemanal", nullable = false)
    private Long cargahorariasemanal;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "disciplina")
    private List<Oferta> ofertas;

    public Disciplina() {
    }

    public Disciplina(int id, Curso curso, String descricao, Long cargahoraria, Long cargahorariasemanal, List<Oferta> ofertas) {
        this.id = id;
        this.curso = curso;
        this.descricao = descricao;
        this.cargahoraria = cargahoraria;
        this.cargahorariasemanal = cargahorariasemanal;
        this.ofertas = ofertas;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getCargahoraria() {
        return cargahoraria;
    }

    public void setCargahoraria(Long cargahoraria) {
        this.cargahoraria = cargahoraria;
    }

    public Long getCargahorariasemanal() {
        return cargahorariasemanal;
    }

    public void setCargahorariasemanal(Long cargahorariasemanal) {
        this.cargahorariasemanal = cargahorariasemanal;
    }

    public List<Oferta> getOfertas() {
        return ofertas;
    }

    public void setOfertas(List<Oferta> ofertas) {
        this.ofertas = ofertas;
    }

    @Override
    public String toString() {
        return descricao;
    }

}
