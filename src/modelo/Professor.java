package modelo;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@NamedQuery(name="listarTodosOsProfessores", query="from Professor")
@Table(name = "professor")
public class Professor {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private int id;

    @Column(name = "descricao", nullable = false)
    private String descricao;

    @Column(name = "cargahorariasemanal", nullable = false)
    private Long cargahorariasemanal;

    @Column(name = "diapreferencial", nullable = false)
    private int diapreferencial;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "professor")
    private List<Oferta> ofertas;

    public Professor() {
    }

    public Professor(int id, String descricao, Long cargahorariasemanal, int diapreferencial) {
        this.id = id;
        this.descricao = descricao;
        this.cargahorariasemanal = cargahorariasemanal;
        this.diapreferencial = diapreferencial;
    }

    public Professor(int id, String descricao, Long cargahorariasemanal, int diapreferencial, List<Oferta> ofertas) {
        this.id = id;
        this.descricao = descricao;
        this.cargahorariasemanal = cargahorariasemanal;
        this.diapreferencial = diapreferencial;
        this.ofertas = ofertas;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getCargahorariasemanal() {
        return cargahorariasemanal;
    }

    public void setCargahorariasemanal(Long cargahorariasemanal) {
        this.cargahorariasemanal = cargahorariasemanal;
    }

    public int getDiapreferencial() {
        return diapreferencial;
    }

    public void setDiapreferencial(int diapreferencial) {
        this.diapreferencial = diapreferencial;
    }

    public List<Oferta> getOfertas() {
        return ofertas;
    }

    public void setOfertas(List<Oferta> ofertas) {
        this.ofertas = ofertas;
    }

    @Override
    public String toString() {
        return descricao;
    }

}
